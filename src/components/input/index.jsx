import React from 'react';

export const Input = ({ onChange, value }) => (
    <input onChange={ onChange } value={ value } />
);
