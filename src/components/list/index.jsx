import React from 'react';

export const List = ({ list }) => (
    <ul>
      { list.map((item, i) => <li key={i}>{ item } </li>) }
    </ul>
);
