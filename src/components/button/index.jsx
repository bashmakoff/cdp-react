import React from 'react';

export const Button = ({ onClick }) => (
    <button onClick={ onClick }>
        Search
    </button>
);
