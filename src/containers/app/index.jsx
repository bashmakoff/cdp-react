import React, { Component } from 'react';
import { List, Button, Input } from '../../components';
import fetchJsonp from 'fetch-jsonp';
const WIKI_ENDPOINT = 'https://en.wikipedia.org/w/api.php';

export class App extends Component{
    constructor() {
        super();
        this.state = {
            list: [],
            value: ''
        }
        this.onChange = this.onChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }
    onChange(ev) {
        let { value } = ev.target;
        this.setState({ value })
    }
    onSearch() {
        fetchJsonp(`${WIKI_ENDPOINT}?action=opensearch&limit=20&search=${this.state.value}&format=json`)
        .then(res => res.json())
        .then(data => {
            if (!data.error) {
                let list = data[1]
                this.setState({ list })
            } else {
                this.setState({ list: [] })
            }
        })
    }
    render() {
        return (
            <div>
                <Input value={ this.state.value } onChange={ this.onChange } />
                <Button onClick={ this.onSearch } />
                <List list={ this.state.list } />
            </div>
        )
    }
}